# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Pavel Borecki <pavel.borecki@gmail.com>, 2019
# fusiondirectory <contact@fusiondirectory.org>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2023-04-25 13:31+0000\n"
"PO-Revision-Date: 2018-08-13 19:52+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2021\n"
"Language-Team: Czech (Czech Republic) (https://app.transifex.com/fusiondirectory/teams/12202/cs_CZ/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs_CZ\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: admin/dns/class_DmarcRecordAttribute.inc:220
msgid "DKIM Identifier Alignment mode"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:221
msgid ""
"Indicates whether strict or relaxed DKIM Identifier Alignment mode is "
"required by the Domain Owner"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:223
#: admin/dns/class_DmarcRecordAttribute.inc:229
msgid "Relaxed"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:223
#: admin/dns/class_DmarcRecordAttribute.inc:229
msgid "Strict"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:226
msgid "SPF Identifier Alignment mode"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:227
msgid ""
"Indicates whether strict or relaxed SPF Identifier Alignment mode is "
"required by the Domain Owner"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:235
msgid "Failure reporting options"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:236
msgid "Provides requested options for generation of failure reports"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:244
msgid "Requested Mail Receiver policy"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:245
#: admin/dns/class_DmarcRecordAttribute.inc:251
msgid ""
"Indicates the policy to be enacted by the Receiver at the request of the "
"Domain Owner"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:250
msgid "Subdomain Mail Receiver policy"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:256
msgid "Percentage of messages"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:257
msgid ""
"Percentage of messages from the Domain Owner's mail stream to which the "
"DMARC policy is to be applied"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:266
msgid "Report interval"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:267
msgid "Interval requested between aggregate reports"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:275
msgid "Addresses for aggregate feedback"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:276
msgid "Addresses to which aggregate feedback is to be sent"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:285
msgid "Addresses for failures"
msgstr ""

#: admin/dns/class_DmarcRecordAttribute.inc:286
msgid ""
"Addresses to which message-specific failure information is to be reported"
msgstr ""

#: admin/dns/class_dnsAcl.inc:26 admin/dns/class_dnsAcl.inc:27
#: admin/dns/class_dnsAcl.inc:30
msgid "DNS acl"
msgstr "ACL seznamy pro DNS"

#: admin/dns/class_dnsAcl.inc:43
msgid "Acl"
msgstr "ACL seznam"

#: admin/dns/class_dnsAcl.inc:47
msgid "ACL name"
msgstr "Název ACL seznamu"

#: admin/dns/class_dnsAcl.inc:47
msgid "Name of this acl"
msgstr "Název tohoto ACL seznamu"

#: admin/dns/class_dnsAcl.inc:52
msgid "Address match list"
msgstr "Seznam shod adresy"

#: admin/dns/class_dnsAcl.inc:52
msgid "The ip address match list for this acl"
msgstr "Seznam IP adres pro porovnávání s tímto ACL seznamem"

#: admin/dns/class_dnsManagement.inc:36 admin/systems/class_dnsHost.inc:152
#: config/dns/class_dnsConfig.inc:41
msgid "DNS"
msgstr "DNS"

#: admin/dns/class_dnsManagement.inc:37
msgid "DNS Management"
msgstr "Správa DNS"

#: admin/dns/class_dnsManagement.inc:38
msgid "Manage DNS zones and views"
msgstr ""

#: admin/dns/class_dnsManagement.inc:40
msgid "Systems"
msgstr "Systémy"

#: admin/dns/class_dnsManagement.inc:54 admin/dns/class_dnsManagement.inc:74
msgid "Refresh Zone"
msgstr ""

#: admin/dns/class_dnsManagement.inc:74
#, php-format
msgid "Invalid target \"%s\" passed for action \"%s\""
msgstr ""

#: admin/dns/class_dnsManagement.inc:82 admin/dns/class_dnsManagement.inc:89
#, php-format
msgid "Could not run ldap2zone: %s"
msgstr ""

#: admin/dns/class_dnsManagement.inc:83
msgid "More than one server matches the SOA"
msgstr "SOA záznamu odpovídá více než jeden server"

#: admin/dns/class_dnsManagement.inc:90
#, php-format
msgid "Could not find the primary server \"%s\""
msgstr ""

#: admin/dns/class_dnsManagement.inc:96 admin/systems/class_dnsHost.inc:344
msgid "Argonaut client needs to be activated to use ldap2zone remotely"
msgstr "Pro použití ldap2zone je třeba, aby byl aktivovaný argonaut klient"

#: admin/dns/class_dnsManagement.inc:105 admin/systems/class_dnsHost.inc:353
msgid "Cannot use argonaut client on a system without a Mac address"
msgstr ""

#: admin/dns/class_dnsManagement.inc:114 admin/systems/class_dnsHost.inc:361
msgid "Ldap2zone"
msgstr "Ldap2zone"

#: admin/dns/class_dnsManagement.inc:114 admin/systems/class_dnsHost.inc:361
#, php-format
msgid "Ldap2Zone called for zone \"%s\""
msgstr "Ldap2Zone zavoláno pro zónu „%s“"

#: admin/dns/class_dnsView.inc:26 admin/dns/class_dnsView.inc:27
#: admin/dns/class_dnsView.inc:30
msgid "DNS view"
msgstr "DNS pohled"

#: admin/dns/class_dnsView.inc:43
msgid "View"
msgstr "Zobrazit"

#: admin/dns/class_dnsView.inc:47
msgid "View name"
msgstr "Zobrazit název"

#: admin/dns/class_dnsView.inc:47
msgid "Name of this view"
msgstr "Název tohoto pohledu"

#: admin/dns/class_dnsView.inc:51
msgid "Match clients ACL"
msgstr "Hledat shodu ACL seznamů klientů"

#: admin/dns/class_dnsView.inc:51
msgid ""
"Name of the ACL to use for the source IP address of the incoming requests"
msgstr ""
"Název ACL seznamu který použít pro zdrojovou IP adresu příchozích požadavků"

#: admin/dns/class_dnsView.inc:55
msgid "Match destinations ACL"
msgstr "Hledat shodu v cílových ACL seznamech"

#: admin/dns/class_dnsView.inc:55
msgid ""
"Name of the ACL to use for the destination IP address of the incoming "
"requests"
msgstr ""
"Název ACL seznamu který použít pro cílovou IP adresu pro příchozí požadavky"

#: admin/dns/class_dnsView.inc:59
msgid "Match recursive only"
msgstr "Hledat shodu pouze rekurzivně"

#: admin/dns/class_dnsView.inc:59
msgid "Match only recursive queries in this view"
msgstr "V tomto pohledu hledat shodu pouze v rekurzivních dotazech"

#: admin/dns/class_dnsView.inc:64 admin/systems/class_dnsHost.inc:171
msgid "DNS zones"
msgstr "DNS zóny"

#: admin/dns/class_dnsView.inc:64
msgid "DNS zones in this view"
msgstr "DNS zóny v tomto pohledu"

#: admin/dns/class_DnsRecordAttribute.inc:133 admin/dns/class_dnsZone.inc:244
msgid "Type"
msgstr "Typ"

#: admin/dns/class_DnsRecordAttribute.inc:136
msgid "Reverse zone"
msgstr "obrácená zóna"

#: admin/dns/class_DnsRecordAttribute.inc:136
msgid "Reverse zone this record should be in, if any"
msgstr "Reverzní zóna ve které by tento záznam měl být (pokud je)"

#: admin/dns/class_DnsRecordAttribute.inc:169
msgid "LOC Record"
msgstr "LOC záznam"

#: admin/dns/class_DnsRecordAttribute.inc:173
msgid "Latitude"
msgstr "Zeměpisná šířka"

#: admin/dns/class_DnsRecordAttribute.inc:177
#: admin/dns/class_DnsRecordAttribute.inc:204
msgid "Degrees"
msgstr "Stupně"

#: admin/dns/class_DnsRecordAttribute.inc:182
#: admin/dns/class_DnsRecordAttribute.inc:209
msgid "Minutes"
msgstr "minuty"

#: admin/dns/class_DnsRecordAttribute.inc:187
#: admin/dns/class_DnsRecordAttribute.inc:214
msgid "Seconds"
msgstr "Sekund"

#: admin/dns/class_DnsRecordAttribute.inc:192
msgid "North/South"
msgstr "Sever/Jih"

#: admin/dns/class_DnsRecordAttribute.inc:195
msgid "North"
msgstr "Sever"

#: admin/dns/class_DnsRecordAttribute.inc:195
msgid "South"
msgstr "Jih"

#: admin/dns/class_DnsRecordAttribute.inc:200
msgid "Longitude"
msgstr "Zeměpisná délka"

#: admin/dns/class_DnsRecordAttribute.inc:219
msgid "East/West"
msgstr "Východ/západ"

#: admin/dns/class_DnsRecordAttribute.inc:222
msgid "East"
msgstr "Východ"

#: admin/dns/class_DnsRecordAttribute.inc:222
msgid "West"
msgstr "Západ"

#: admin/dns/class_DnsRecordAttribute.inc:227
msgid "Altitude (meters)"
msgstr "Nadmořská výška (v metrech)"

#: admin/dns/class_DnsRecordAttribute.inc:232
msgid "Size (meters)"
msgstr "Velikost (v metrech)"

#: admin/dns/class_DnsRecordAttribute.inc:237
msgid "Horizontal precision (meters)"
msgstr "Přesnost vodorovně (metry)"

#: admin/dns/class_DnsRecordAttribute.inc:242
msgid "Vertical precision (meters)"
msgstr "Přesnost svisle (metry)"

#: admin/dns/class_DnsRecordAttribute.inc:253
msgid "NAPTR Record"
msgstr "NAPTR záznam"

#: admin/dns/class_DnsRecordAttribute.inc:257
msgid "Order"
msgstr "Pořadí"

#: admin/dns/class_DnsRecordAttribute.inc:257
msgid ""
"Integer specifying the order in which the NAPTR records MUST be processed to"
" ensure the correct ordering of rules.  Low numbers are processed before "
"high numbers."
msgstr ""
"Celé číslo které určuje pořadí ve kterém MUSÍ být zpracovávány NAPTR záznamy"
" se stejnými „order“ (pořadí) hodnotami, nízká čísla jsou zpracovány před "
"těmi vyššími."

#: admin/dns/class_DnsRecordAttribute.inc:262
msgid "Preference"
msgstr "Předvolby"

#: admin/dns/class_DnsRecordAttribute.inc:262
msgid ""
"Integer that specifies the order in which NAPTR records with equal \"order\""
" values SHOULD be processed, low numbers being processed before high "
"numbers."
msgstr ""
"Celé číslo které určuje pořadí ve kterém BY MĚLY být zpracovávány NAPTR "
"záznamy se stejnými „order“ (pořadí) hodnotami, nízká čísla jsou zpracovány "
"před těmi vyššími."

#: admin/dns/class_DnsRecordAttribute.inc:267
#: admin/dns/class_DnsRecordAttribute.inc:358
#: admin/dns/class_DkimRecordAttribute.inc:152
msgid "Flags"
msgstr "Příznaky"

#: admin/dns/class_DnsRecordAttribute.inc:267
msgid ""
"Flags to control aspects of the rewriting and interpretation of the fields "
"in the record. Flags are single characters from the set [A-Z0-9].  The case "
"of the alphabetic characters is not significant."
msgstr ""
"Příznaky pro ovládání aspektů přepisování a interpretace kolonek v záznamu. "
"Příznaky jsou jednoznakovové z množiny A až Z (bez diakritiky) a 0 až 9. "
"Velikost písmen není důležitá."

#: admin/dns/class_DnsRecordAttribute.inc:271
msgid "Service"
msgstr "Služba"

#: admin/dns/class_DnsRecordAttribute.inc:271
msgid ""
"Specifies the service(s) available down this rewrite path. It may also "
"specify the particular protocol that is used to talk with a service. A "
"protocol MUST be specified if the flags field states that the NAPTR is "
"terminal."
msgstr ""
"Určuje služby dostupné v této rewrite cestě. Může také určovat konkrétní "
"protokol který je použit pro komunikaci se službou. JE NUTNÉ zadat protokol "
"pokud kolonka příznak uvádí, že NAPTR je terminál."

#: admin/dns/class_DnsRecordAttribute.inc:275
msgid "Regular Expression"
msgstr "Regulární výraz"

#: admin/dns/class_DnsRecordAttribute.inc:275
msgid ""
"A STRING containing a substitution expression that is applied to the "
"original string held by the client in order to construct the next domain "
"name to lookup."
msgstr ""
"ŘETĚZEC obsahující nahrazující výraz který bude použit na původní řetězec "
"držený klientem aby vytvořil další doménový název který hledat."

#: admin/dns/class_DnsRecordAttribute.inc:279
msgid "Replacement"
msgstr "Nahrazení"

#: admin/dns/class_DnsRecordAttribute.inc:279
msgid ""
"The next NAME to query for NAPTR, SRV, or address records depending on the "
"value of the flags field."
msgstr ""
"Příští NAME kterého se dotazovat na záznamy NAPTR, SRV nebo adresy, v "
"závislosti na hodnotě kolonky příznaků."

#: admin/dns/class_DnsRecordAttribute.inc:290
msgid "SRV Record"
msgstr "SRV záznam"

#: admin/dns/class_DnsRecordAttribute.inc:294
#: admin/dns/class_DnsRecordAttribute.inc:392
msgid "Priority"
msgstr "Priorita"

#: admin/dns/class_DnsRecordAttribute.inc:294
msgid "Priority of the target host, lower value means more preferred"
msgstr "Priorita cílového stroje, nižší hodnota znamená více upřednostňované"

#: admin/dns/class_DnsRecordAttribute.inc:299
msgid "Weight"
msgstr "Důležitost"

#: admin/dns/class_DnsRecordAttribute.inc:299
msgid ""
"Relative weight for records with the same priority, higher value means more "
"preferred"
msgstr ""
"Relativní důležitost pro záznamy se stejnou prioritou, vyšší hodnota znamená"
" více upřednostňované"

#: admin/dns/class_DnsRecordAttribute.inc:304
msgid "Port"
msgstr "port"

#: admin/dns/class_DnsRecordAttribute.inc:304
msgid "TCP or UDP port on which the service is to be found"
msgstr "TCP nebo UDP port na kterém se služba nachází"

#: admin/dns/class_DnsRecordAttribute.inc:309
#: admin/dns/class_DnsRecordAttribute.inc:340
#: admin/dns/class_DnsRecordAttribute.inc:347
#: admin/dns/class_DnsRecordAttribute.inc:397
#: admin/dns/class_DnsRecordAttribute.inc:409
msgid "Target"
msgstr "Cíl"

#: admin/dns/class_DnsRecordAttribute.inc:309
msgid ""
"Canonical hostname of the machine providing the service, ending in a dot"
msgstr "Kanonický název stroje poskytujícího službu, zakončený tečkou"

#: admin/dns/class_DnsRecordAttribute.inc:324
msgid "DKIM Record"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:329
msgid "DMARC Record"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:340
msgid "An IPv4 address"
msgstr "IPv4 adresa"

#: admin/dns/class_DnsRecordAttribute.inc:347
msgid "An IPv6 address"
msgstr "IPv6 adresa"

#: admin/dns/class_DnsRecordAttribute.inc:354
msgid "CAA Record"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:358
msgid ""
"Integer representing flags. As of 2021 it should be either 0 or 128 for "
"\"issuer critical\" flag."
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:364
msgid "Tag"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:364
msgid "Which kind of CAA property you want to set"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:369
msgid "Value"
msgstr "Hodnota"

#: admin/dns/class_DnsRecordAttribute.inc:369
msgid "Value for the specified tag, without quotes"
msgstr ""

#: admin/dns/class_DnsRecordAttribute.inc:388
msgid "MX Record"
msgstr "MX záznam"

#: admin/dns/class_DnsRecordAttribute.inc:392
msgid ""
"Preference given to this RR among others at the same owner, lower values are"
" preferred"
msgstr ""
"Přednost daná tomuto RR mezi ostatními při stejném vlastníkovi, nižší "
"hodnoty znamenají více upřednostňované"

#: admin/dns/class_DnsRecordAttribute.inc:397
msgid ""
"Domain name which specifies a host willing to act as a mail exchange for the"
" owner name"
msgstr ""
"Doménový název který určuje stroj ochotný fungovat jako vyměňovač e-mailů "
"pro název vlastníka"

#: admin/dns/class_DnsRecordAttribute.inc:409
msgid ""
"Domain name which specifies a host which should be authoritative for the "
"specified class and domain"
msgstr ""
"Doménový název který určuje stroj který by měl být autoritativní pro zadanou"
" třídu a doménu"

#: admin/dns/class_DnsRecordAttribute.inc:421 admin/dns/class_dnsZone.inc:46
#: admin/dns/class_dnsZone.inc:55
msgid "Record"
msgstr "Záznam"

#: admin/dns/class_DnsRecordAttribute.inc:421
msgid ""
"SSHFP record content. Can be obtained using \"ssh-keygen -r some.host.tld\","
" or sshfp command for instance"
msgstr ""
"Obsah SSHFP záznamu. Je možné získat s použitím „ssh-keygen -r "
"nejaky.stroj.tld“, nebo sshfp příkaz například"

#: admin/dns/class_DnsRecordAttribute.inc:427
msgid "Redirect to"
msgstr "Přesměrovat na"

#: admin/dns/class_DnsRecordAttribute.inc:427
msgid "Domain that this subdomain is an alias of"
msgstr "Doména které je tato poddoména alternativním názvem"

#: admin/dns/class_DnsRecordAttribute.inc:433 admin/dns/class_dnsZone.inc:245
msgid "Content"
msgstr "Obsah"

#: admin/dns/class_DnsRecordAttribute.inc:433
msgid "Content of this record"
msgstr "Obsah tohoto záznamu"

#: admin/dns/class_DnsRecordAttribute.inc:485
msgid "The entered IP does not match the selected reverse zone"
msgstr "Zadaná IP adresa neodpovídá zvolené reverzní zóně"

#: admin/dns/class_dnsZone.inc:28
msgid "DNS Record domain and type"
msgstr "Doména a typ DNS záznamu"

#: admin/dns/class_dnsZone.inc:31
#, php-format
msgid "%s record"
msgstr "%s záznam"

#: admin/dns/class_dnsZone.inc:35 admin/dns/class_dnsZone.inc:36
msgid "DNS record"
msgstr "DNS záznam"

#: admin/dns/class_dnsZone.inc:50 admin/dns/class_dnsZone.inc:243
msgid "Subdomain"
msgstr "Poddoména"

#: admin/dns/class_dnsZone.inc:50
msgid "Relative subdomain name"
msgstr "Relativní název poddomény"

#: admin/dns/class_dnsZone.inc:55
msgid "DNS Record"
msgstr "DNS záznam"

#: admin/dns/class_dnsZone.inc:246
msgid "Reverse"
msgstr "Obráceně"

#: admin/dns/class_dnsZone.inc:440
#, php-format
msgid "The IP %s does not match the selected reverse %s, it has been ignored"
msgstr "IP adresa %s neodpovídá zvolené reverzi %s, bude ignorováno"

#: admin/dns/class_dnsZone.inc:748
#, php-format
msgid ""
"\"%s\" must contain a domain name in lowercase and end with a final "
"dot.<br/><br/>Example: example.com."
msgstr ""
"Je třeba, aby „%s“ obsahovalo název domény malými písmeny a končilo "
"tečkou.<br/><br/>Příklad: example.com."

#: admin/dns/class_dnsZone.inc:751
#, php-format
msgid ""
"\"%s\" must contain a domain name in lowercase.<br/><br/>Example: "
"example.com"
msgstr ""
"Je třeba, aby „%s“ obsahovalo název domény malými písmeny.<br/><br/>Příklad:"
" example.com."

#: admin/dns/class_dnsZone.inc:770 admin/dns/class_dnsZone.inc:771
#: admin/dns/class_dnsZone.inc:775
msgid "DNS zone"
msgstr "DNS zóna"

#: admin/dns/class_dnsZone.inc:790
msgid "Zone"
msgstr "Zóna"

#: admin/dns/class_dnsZone.inc:794
msgid "Zone name"
msgstr "Název zóny"

#: admin/dns/class_dnsZone.inc:804
msgid "Reverse zones"
msgstr "Obrácené zóny"

#: admin/dns/class_dnsZone.inc:804
#, php-format
msgid ""
"Reverse zones for this zone in the form xx.xx.in-addr.arpa%1$s or "
"x.x.ip6.arpa%1$s"
msgstr ""
"Reverzní zóny pro tuto zónu v podobě xx.xx.in-addr.arpa%1$s nebo "
"x.x.ip6.arpa%1$s"

#: admin/dns/class_dnsZone.inc:817
msgid "SOA record"
msgstr "SOA záznam"

#: admin/dns/class_dnsZone.inc:820
msgid "SOA Record"
msgstr "SOA záznam"

#: admin/dns/class_dnsZone.inc:824
msgid "Primary DNS server"
msgstr "Hlavní DNS server"

#: admin/dns/class_dnsZone.inc:824
msgid ""
"Domain name of the name server that was the original or primary source of "
"data for this zone"
msgstr ""
"Doménový název jmenného serveru který byl původním nebo hlavním zdrojem "
"údajů pro tuto zónu"

#: admin/dns/class_dnsZone.inc:828
msgid "Mail address"
msgstr "e-mailová adresa"

#: admin/dns/class_dnsZone.inc:828
msgid ""
"Domain name which specifies the mailbox of the person responsible for this "
"zone"
msgstr ""
"Doménový název který určuje e-mailovou schránku osoby odpovědné za tuto zónu"

#: admin/dns/class_dnsZone.inc:832
msgid "Serial number"
msgstr "Sériové číslo"

#: admin/dns/class_dnsZone.inc:832
msgid "Version number of the original copy of the zone"
msgstr "Číslo verze původní kopie zóny"

#: admin/dns/class_dnsZone.inc:837
msgid "Refresh"
msgstr "obnovit"

#: admin/dns/class_dnsZone.inc:837
msgid "Time interval before the zone should be refreshed"
msgstr "Časový interval před kterým by zóna měla být občerstvena"

#: admin/dns/class_dnsZone.inc:842
msgid "Retry"
msgstr "znovu"

#: admin/dns/class_dnsZone.inc:842
msgid ""
"Time interval that should elapse before a failed refresh should be retried"
msgstr ""
"Časový interval který by měl uplynout před opětovným pokusem po předchozí "
"nezdařené aktualizaci"

#: admin/dns/class_dnsZone.inc:847
msgid "Expire"
msgstr "Platnost skončí"

#: admin/dns/class_dnsZone.inc:847
msgid ""
"Time value that specifies the upper limit on the time interval that can "
"elapse before the zone is no longer authoritative"
msgstr ""
"Časová hodnota která určuje horní limit časového intravalu až po jehož "
"uplynutí zóna už nebude autoritativní"

#: admin/dns/class_dnsZone.inc:852
msgid "TTL"
msgstr "TTL"

#: admin/dns/class_dnsZone.inc:852
msgid "Minimum TTL field that should be exported with any RR from this zone"
msgstr ""
"Kolonka minimálního TTL která by měla být exportována s libovolným RR z této"
" zóny"

#: admin/dns/class_dnsZone.inc:863
msgid "Records"
msgstr "Záznamy"

#: admin/dns/class_dnsZone.inc:867
msgid "The DNS records for this zone"
msgstr "DNS záznamy pro tuto zónu"

#: admin/dns/class_DkimRecordAttribute.inc:127
msgid "Hash algorithms"
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:128
msgid ""
"A colon-separated list of hash algorithms that might be used, defaults to "
"allowing all algorithms."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:132
msgid "Key type"
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:133
msgid "Key type of the public key, default is \"rsa\"."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:137
msgid "Notes"
msgstr "Poznámky"

#: admin/dns/class_DkimRecordAttribute.inc:138
msgid ""
"Notes that might be of interest to a human. No interpretation is made by any"
" program."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:142
msgid "Public key"
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:143
msgid ""
"Public-key data encoded in base64. An empty value means that this public key"
" has been revoked."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:147
msgid "Service type"
msgstr "Typ služby"

#: admin/dns/class_DkimRecordAttribute.inc:148
msgid ""
"A colon-separated list of service types to which this record applies, "
"default is \"*\"."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:153
msgid ""
"Flags, represented as a colon-separated list of names, default is no flags "
"set."
msgstr ""

#: admin/dns/class_DkimRecordAttribute.inc:235
msgid "The \"Public key\" field does not contain valid base64 data"
msgstr ""

#: admin/dns/class_SpfRecordAttribute.inc:27
msgid "Terms"
msgstr ""

#: admin/dns/class_SpfRecordAttribute.inc:80
#, php-format
msgid "\"%s\" is not a valid directive for an SPF record"
msgstr ""

#: admin/dns/class_SpfRecordAttribute.inc:88
msgid "Directive \"all\" does not accept a value in an SPF record"
msgstr ""

#: admin/dns/class_SpfRecordAttribute.inc:99
#, php-format
msgid "Directive \"%s\" needs a value in an SPF record"
msgstr ""

#: admin/dns/class_SpfRecordAttribute.inc:118
#, php-format
msgid "\"%s\" is not a valid modifier for an SPF record"
msgstr ""

#: admin/systems/class_dnsHost.inc:127 admin/systems/class_dnsHost.inc:418
#, php-format
msgid ""
"\"%s\" is locked (by \"%s\" since %s), could not save modifications to this "
"DNS zone"
msgstr ""

#: admin/systems/class_dnsHost.inc:153
msgid "Edit the DNS zones of a system"
msgstr "Upravit DNS zóny systému"

#: admin/systems/class_dnsHost.inc:175
msgid "DNS zones for this host"
msgstr "DNS zóny pro tento stroj"

#: admin/systems/class_dnsHost.inc:182
msgid "SOA records"
msgstr "SOA záznamy"

#: admin/systems/class_dnsHost.inc:186 admin/systems/class_dnsHost.inc:262
msgid "DNS Records"
msgstr "DNS záznamy"

#: admin/systems/class_dnsHost.inc:258
msgid "Primary servers"
msgstr "Hlavní servery"

#: admin/systems/class_dnsHost.inc:277
#, php-format
msgid "The DNS records for zone \"%s\""
msgstr "DNS záznamy pro zónu „%s“"

#: admin/systems/class_dnsHost.inc:292
msgid "Refresh zone file"
msgstr ""

#: admin/systems/class_dnsHost.inc:299
#, php-format
msgid "The primary server for zone \"%s\""
msgstr "Hlavní server pro zónu „%s“"

#: admin/systems/class_dnsHost.inc:450 admin/systems/class_dnsHost.inc:468
#, php-format
msgid "%d records were updated from %s to %s in zone %s"
msgstr "%d záznamů bylo aktualizováno z %s do %s v zóně %s"

#: admin/systems/class_dnsHost.inc:482 admin/systems/class_dnsHost.inc:513
msgid "DNS update"
msgstr "Aktualizace DNS"

#: admin/systems/class_dnsHost.inc:566 admin/systems/class_dnsHost.inc:587
#, php-format
msgid "%d records were removed in zone %s"
msgstr "%d záznamů bylo odebráno v zóně %s"

#: config/dns/class_dnsConfig.inc:27
msgid "DNS configuration"
msgstr "Nastavení DNS"

#: config/dns/class_dnsConfig.inc:28
msgid "FusionDirectory dns plugin configuration"
msgstr "Nastavení zásuvného modulu dns pro FusionDirectory"

#: config/dns/class_dnsConfig.inc:44
msgid "DNS RDN"
msgstr "Relativní rozlišený název DNS"

#: config/dns/class_dnsConfig.inc:44
msgid "Branch in which DNS zones will be stored"
msgstr "Větev ve které budou ukládány DNS záznamy"

#: config/dns/class_dnsConfig.inc:49
msgid "Store final dot in domains"
msgstr "Domény ukládat včetně teček na samém konci"

#: config/dns/class_dnsConfig.inc:49
msgid "Should FD store a final dot at the end of domains?"
msgstr "Má F.D ukládat tečku úplně na konci domény?"

#: admin/systems/dnsrecords.tpl.c:2
msgid "Insufficient rights"
msgstr "Nedostačující oprávnění"

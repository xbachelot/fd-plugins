# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2020
# Choi Chris <chulwon.choi@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2023-04-25 13:25+0000\n"
"PO-Revision-Date: 2018-08-13 20:05+0000\n"
"Last-Translator: Choi Chris <chulwon.choi@gmail.com>, 2021\n"
"Language-Team: Korean (https://app.transifex.com/fusiondirectory/teams/12202/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: admin/systems/class_phoneGeneric.inc:28
#: admin/systems/class_phoneGeneric.inc:34
#: admin/systems/class_phoneGeneric.inc:66
#: admin/systems/class_mobilePhoneGeneric.inc:69
msgid "Phone"
msgstr "전화번호"

#: admin/systems/class_phoneGeneric.inc:29
msgid "Phone information"
msgstr "전화 정보"

#: admin/systems/class_phoneGeneric.inc:35
msgid "Phone hardware"
msgstr "전화 하드웨어"

#: admin/systems/class_phoneGeneric.inc:50
#: admin/systems/class_printGeneric.inc:57
#: admin/systems/class_workstationGeneric.inc:56
#: admin/systems/class_componentGeneric.inc:51
#: admin/systems/class_mobilePhoneGeneric.inc:53
msgid "Properties"
msgstr "설정"

#: admin/systems/class_phoneGeneric.inc:54
#: admin/systems/class_printGeneric.inc:61
#: admin/systems/interfaces/class_networkInterface.inc:54
#: admin/systems/class_workstationGeneric.inc:60
#: admin/systems/services/shares/class_serviceShare.inc:50
#: admin/systems/class_componentGeneric.inc:55
#: admin/systems/class_mobilePhoneGeneric.inc:57
msgid "Name"
msgstr "명칭"

#: admin/systems/class_phoneGeneric.inc:54
msgid "The name of the phone"
msgstr "전화의 이름"

#: admin/systems/class_phoneGeneric.inc:58
#: admin/systems/class_printGeneric.inc:65
#: admin/systems/class_workstationGeneric.inc:65
#: admin/systems/services/shares/class_serviceShare.inc:54
#: admin/systems/class_componentGeneric.inc:59
#: admin/systems/class_mobilePhoneGeneric.inc:61
msgid "Description"
msgstr "설명"

#: admin/systems/class_phoneGeneric.inc:58
msgid "A short description of the phone"
msgstr "전화에 대한 간단한 설명"

#: admin/systems/class_phoneGeneric.inc:69
#: admin/systems/class_mobilePhoneGeneric.inc:72
msgid "Serial Number"
msgstr "일련번호"

#: admin/systems/class_phoneGeneric.inc:69
msgid "The serial number of the phone"
msgstr "전화에 대한 일련번호"

#: admin/systems/class_phoneGeneric.inc:73
#: admin/systems/class_mobilePhoneGeneric.inc:90
msgid "Telephone Number"
msgstr "전화 번호"

#: admin/systems/class_phoneGeneric.inc:73
msgid "The telephone number of the phone"
msgstr "전화에 대한 전화번호"

#: admin/systems/class_terminalStartup.inc:29
msgid "Startup"
msgstr "시작"

#: admin/systems/class_terminalStartup.inc:30
msgid "Terminal startup"
msgstr "터미널 시작"

#: admin/systems/class_terminalStartup.inc:47
msgid "Startup parameters"
msgstr "시작 매개 변수"

#: admin/systems/class_terminalStartup.inc:50
msgid "Root server"
msgstr "루트 서버"

#: admin/systems/class_terminalStartup.inc:50
msgid "The root server the terminal should be using"
msgstr "터미널이 사용해야하는 루트 서버"

#: admin/systems/class_terminalStartup.inc:54
msgid "Swap server"
msgstr "스왑 서버"

#: admin/systems/class_terminalStartup.inc:54
msgid "The swap server the terminal should be using"
msgstr "터미널이 사용해야하는 스왑 서버"

#: admin/systems/class_terminalStartup.inc:60
msgid "Remote desktop"
msgstr "원격 데스크탑"

#: admin/systems/class_terminalStartup.inc:64
msgid "Connect method"
msgstr "연결 방법"

#: admin/systems/class_terminalStartup.inc:68
#: admin/systems/services/terminal/class_serviceTerminal.inc:58
msgid "XDMCP"
msgstr "XDMCP"

#: admin/systems/class_terminalStartup.inc:68
#: admin/systems/services/terminal/class_serviceTerminal.inc:58
msgid "LDM"
msgstr "LDM"

#: admin/systems/class_terminalStartup.inc:68
#: admin/systems/services/terminal/class_serviceTerminal.inc:58
msgid "Shell"
msgstr "쉘"

#: admin/systems/class_terminalStartup.inc:68
#: admin/systems/services/terminal/class_serviceTerminal.inc:58
msgid "Telnet"
msgstr "텔넷"

#: admin/systems/class_terminalStartup.inc:68
#: admin/systems/services/terminal/class_serviceTerminal.inc:58
msgid "Windows RDP"
msgstr "Windows RDP"

#: admin/systems/class_terminalStartup.inc:71
msgid "Terminal server"
msgstr "터미널 서버"

#: admin/systems/class_terminalStartup.inc:106
#: admin/systems/class_terminalStartup.inc:117
msgid "inherited"
msgstr "계승된"

#: admin/systems/class_terminalStartup.inc:114
msgid "Local swap"
msgstr "로컬 스왑"

#: admin/systems/class_terminalGeneric.inc:26
#: admin/systems/class_terminalGeneric.inc:32
#: admin/systems/class_terminalGeneric.inc:33
msgid "Terminal"
msgstr "터미널"

#: admin/systems/class_terminalGeneric.inc:27
msgid "Terminal information"
msgstr "터미널 정보"

#: admin/systems/class_terminalGeneric.inc:50
msgid "terminal"
msgstr "터미널"

#: admin/systems/class_printGeneric.inc:27
#: admin/systems/class_printGeneric.inc:34
#: admin/systems/class_printGeneric.inc:35
msgid "Printer"
msgstr "프린터"

#: admin/systems/class_printGeneric.inc:28
msgid "Printer information"
msgstr "프린터 정보"

#: admin/systems/class_printGeneric.inc:61
msgid "The name of the printer"
msgstr "프린터의 이름"

#: admin/systems/class_printGeneric.inc:65
msgid "A short description of the printer"
msgstr "프린터에 대한 간단한 설명"

#: admin/systems/class_printGeneric.inc:71
msgid "Details"
msgstr "세부정보"

#: admin/systems/class_printGeneric.inc:74
msgid "Printer location"
msgstr "프린터 위치"

#: admin/systems/class_printGeneric.inc:74
msgid "The location of the printer"
msgstr "프린터의 위치"

#: admin/systems/class_printGeneric.inc:78
msgid "Printer URL"
msgstr "프린터 URL"

#: admin/systems/class_printGeneric.inc:78
msgid "The URL of the printer"
msgstr "프린터의 URL"

#: admin/systems/class_printGeneric.inc:84
#: admin/systems/class_printGeneric.inc:87
msgid "Users which are allowed to use this printer"
msgstr "이 프린터를 사용할 수 있는 사용자"

#: admin/systems/class_printGeneric.inc:93
#: admin/systems/class_printGeneric.inc:96
msgid "Users which are allowed to administrate this printer"
msgstr "이 프린터를 관리 할 수 있는 사용자"

#: admin/systems/class_printGeneric.inc:104
msgid "Windows paths"
msgstr "Windows 경로"

#: admin/systems/class_printGeneric.inc:108
msgid "Inf file"
msgstr "Inf 파일"

#: admin/systems/class_printGeneric.inc:108
msgid "Path to windows inf file for this printer"
msgstr "이 프린터의 Windows inf 파일 경로"

#: admin/systems/class_printGeneric.inc:112
msgid "Driver directory"
msgstr "드라이버 경로"

#: admin/systems/class_printGeneric.inc:112
msgid "Path to directory that contains windows drivers for this printer"
msgstr "이 프린터의 Windows 드라이버가있는 디렉토리의 경로"

#: admin/systems/class_printGeneric.inc:116
msgid "Driver name"
msgstr "드라이버 이름"

#: admin/systems/class_printGeneric.inc:116
msgid "Windows name of the printer driver"
msgstr "프린터 드라이버의 Windows 이름"

#: admin/systems/interfaces/class_networkInterface.inc:29
msgid "Interface"
msgstr "인터페이스"

#: admin/systems/interfaces/class_networkInterface.inc:30
#: admin/systems/interfaces/class_networkInterface.inc:33
msgid "Network interface"
msgstr "네트워크 인터페이스"

#: admin/systems/interfaces/class_networkInterface.inc:51
msgid "General information"
msgstr "일반 정보"

#: admin/systems/interfaces/class_networkInterface.inc:54
msgid "Interface name"
msgstr "인터페이스 이름"

#: admin/systems/interfaces/class_networkInterface.inc:58
msgid "MAC address"
msgstr "맥 주소"

#: admin/systems/interfaces/class_networkInterface.inc:58
msgid "MAC address of this system"
msgstr "시스템의 맥 주소"

#: admin/systems/interfaces/class_networkInterface.inc:63
msgid "IP address"
msgstr "IP 주소"

#: admin/systems/interfaces/class_networkInterface.inc:63
msgid "IP addresses this system uses (v4 or v6)"
msgstr "이 시스템에서 사용하는 IP 주소 (v4 또는 v6)"

#: admin/systems/interfaces/class_networkInterface.inc:72
msgid "IPAM"
msgstr "IPAM"

#: admin/systems/interfaces/class_networkInterface.inc:75
msgid "VLAN"
msgstr "VLAN"

#: admin/systems/interfaces/class_networkInterface.inc:80
msgid "VLAN tagged"
msgstr "태그된 VLAN"

#: admin/systems/interfaces/class_networkInterface.inc:80
msgid "Whether this interface is going to send/receive tagged frames"
msgstr "이 인터페이스가 태그가 지정된 프레임을 전송 / 수신할지 여부"

#: admin/systems/interfaces/class_networkInterface.inc:84
msgid "Subnet"
msgstr "서브넷"

#: admin/systems/interfaces/class_networkInterface.inc:88
msgid "Add free IP from subnet"
msgstr "서브넷에서 사용 가능한  IP 추가"

#: admin/systems/interfaces/class_networkInterface.inc:88
msgid "Adds a new IP from selected subnet"
msgstr "선택한 서브넷에서 새 IP를 추가"

#: admin/systems/interfaces/class_networkInterface.inc:89
msgid "Add IP"
msgstr "IP 추가"

#: admin/systems/interfaces/class_networkInterface.inc:149
msgid "No subnet selected"
msgstr "선택한 서브넷이 없습니다."

#: admin/systems/interfaces/class_networkInterface.inc:155
msgid "Could not find selected subnet"
msgstr "선택한 서브넷을 찾을 수 없습니다."

#: admin/systems/interfaces/class_networkInterface.inc:161
msgid "Selected subnet has no IP"
msgstr "선택한 서브넷에 IP가 없습니다."

#: admin/systems/interfaces/class_networkInterface.inc:182
msgid "Could not find any free IP in the selected subnet"
msgstr "선택한 서브넷에서 사용 가능한 IP를 찾을 수 없습니다."

#: admin/systems/interfaces/class_interfacesManagement.inc:68
#: admin/systems/interfaces/class_interfacesManagement.inc:95
msgid "Interfaces"
msgstr "인터페이스"

#: admin/systems/interfaces/class_interfacesManagement.inc:69
msgid "Edit the network interfaces of a system"
msgstr "시스템의 네트워크 인터페이스 편집"

#: admin/systems/interfaces/class_interfacesManagement.inc:287
#, php-format
msgid "Interfaces \"%s\" and \"%s\" use the same MAC + VLAN combination"
msgstr "\"%s\" 및 \"%s\" 인터페이스는 동일한 MAC + VLAN 조합을 사용합니다."

#: admin/systems/interfaces/class_interfacesManagement.inc:293
#, php-format
msgid "Interfaces \"%s\" and \"%s\" use the same IP address \"%s\""
msgstr "\"%s\" 및 \"%s\" 인터페이스는 동일한 IP 주소 \"%s\"를 사용합니다."

#: admin/systems/interfaces/class_interfacesManagement.inc:386
#, php-format
msgid "The entry %s is not existing"
msgstr "%s 항목이 없습니다."

#: admin/systems/interfaces/class_interfacesManagement.inc:562
#, php-format
msgid "Unknown field \"%s\""
msgstr "잘못된 필드 \"%s\""

#: admin/systems/class_workstationGeneric.inc:27
#: admin/systems/class_workstationGeneric.inc:33
#: admin/systems/class_workstationGeneric.inc:34
msgid "Workstation"
msgstr "워크스테이션"

#: admin/systems/class_workstationGeneric.inc:28
msgid "Workstation information"
msgstr "워크스테이션 정보"

#: admin/systems/class_workstationGeneric.inc:49
msgid "workstation"
msgstr "워크스테이션"

#: admin/systems/class_workstationGeneric.inc:61
#, php-format
msgid "The name of the %s"
msgstr "%s의 이름"

#: admin/systems/class_workstationGeneric.inc:66
#, php-format
msgid "A short description of the %s"
msgstr "%s의 간단한 설명"

#: admin/systems/class_workstationGeneric.inc:70
msgid "Location"
msgstr "위치"

#: admin/systems/class_workstationGeneric.inc:71
#, php-format
msgid "The location of the %s"
msgstr "%s의 위치"

#: admin/systems/class_workstationGeneric.inc:75
#, php-format
msgid "Lock this %s"
msgstr "%s 잠금"

#: admin/systems/class_workstationGeneric.inc:76
#, php-format
msgid "This will prevent the %s from being reinstalled"
msgstr "이렇게하면 %s이 다시 설치되지 않습니다."

#: admin/systems/services/class_servicesManagement.inc:71
#: admin/systems/services/shares/class_serviceShare.inc:28
#: admin/systems/services/ldap/class_serviceLDAP.inc:29
#: admin/systems/services/terminal/class_serviceTerminal.inc:28
msgid "Services"
msgstr "서비스"

#: admin/systems/services/class_servicesManagement.inc:72
msgid "Server services"
msgstr "서버 서비스"

#: admin/systems/services/class_servicesManagement.inc:128
msgid "Edit service"
msgstr "서비스 편집"

#: admin/systems/services/class_servicesManagement.inc:137
msgid "Remove"
msgstr "제거"

#: admin/systems/services/class_servicesManagement.inc:155
msgid "Export list"
msgstr "목록 내보내기"

#: admin/systems/services/class_servicesManagement.inc:164
msgid "Get status"
msgstr "상태 가져 오기"

#: admin/systems/services/class_servicesManagement.inc:173
msgid "Start"
msgstr "시작"

#: admin/systems/services/class_servicesManagement.inc:182
msgid "Stop"
msgstr "종료"

#: admin/systems/services/class_servicesManagement.inc:191
msgid "Restart"
msgstr "재시작"

#: admin/systems/services/class_servicesManagement.inc:242
msgid "Create"
msgstr "생성"

#: admin/systems/services/class_servicesManagement.inc:358
msgid "Information"
msgstr "정보"

#: admin/systems/services/class_servicesManagement.inc:358
msgid "Cannot update service status until it has been saved!"
msgstr "저장 될 때까지 서비스 상태를 업데이트 할 수 없습니다!"

#: admin/systems/services/class_servicesManagement.inc:366
#, php-format
msgid "Unknown action \"%s\""
msgstr "알 수없는 작업 \"%s\""

#: admin/systems/services/class_servicesManagement.inc:397
#, php-format
msgid ""
"Could not get execute action %s on service %s: This server has no mac "
"address."
msgstr "%s  서비스에 대한 %s 실행 작업을 가져올 수 없습니다: 이 서버에는 MAC 주소가 없습니다."

#: admin/systems/services/shares/class_serviceShare.inc:27
#: admin/systems/services/shares/class_serviceShare.inc:28
msgid "Share service"
msgstr "서비스 공유"

#: admin/systems/services/shares/class_serviceShare.inc:41
msgid "Shares"
msgstr "공유"

#: admin/systems/services/shares/class_serviceShare.inc:46
msgid "Shares this server hosts"
msgstr "이 서버 호스트를 공유합니다."

#: admin/systems/services/shares/class_serviceShare.inc:50
msgid "Name of this share"
msgstr "이 공유의 이름"

#: admin/systems/services/shares/class_serviceShare.inc:54
msgid "Description of this share"
msgstr "이 공유에 대한 설명"

#: admin/systems/services/shares/class_serviceShare.inc:58
#: admin/systems/class_systemImport.inc:48
msgid "Type"
msgstr "타입"

#: admin/systems/services/shares/class_serviceShare.inc:58
msgid "Type of share"
msgstr "공유 유형"

#: admin/systems/services/shares/class_serviceShare.inc:63
msgid "Codepage"
msgstr "코드 페이지"

#: admin/systems/services/shares/class_serviceShare.inc:63
msgid "Codepage for this share"
msgstr "이 공유에 대한 코드 페이지"

#: admin/systems/services/shares/class_serviceShare.inc:67
msgid "Path / Volume"
msgstr "경로 / 볼륨"

#: admin/systems/services/shares/class_serviceShare.inc:67
msgid "Path or volume concerned by this share"
msgstr "이 공유와 관련된 경로 또는 볼륨"

#: admin/systems/services/shares/class_serviceShare.inc:71
msgid "Option"
msgstr "옵션"

#: admin/systems/services/shares/class_serviceShare.inc:71
msgid "Special option(s) for this share"
msgstr "이 공유에 대한 특별 옵션(들)"

#: admin/systems/services/shares/class_serviceShare.inc:104
msgid "You need to configure encodings first"
msgstr "먼저 인코딩을 구성해야합니다."

#: admin/systems/services/shares/class_serviceShare.inc:112
msgid "Warning"
msgstr "ㅗ"

#: admin/systems/services/shares/class_serviceShare.inc:112
#, php-format
msgid "Invalid value in encodings : %s"
msgstr "잘못된 인코딩 값 : %s"

#: admin/systems/services/ldap/class_serviceLDAP.inc:28
#: admin/systems/services/ldap/class_serviceLDAP.inc:44
msgid "LDAP service"
msgstr "LDAP 서비스"

#: admin/systems/services/ldap/class_serviceLDAP.inc:29
msgid "LDAP"
msgstr "LDAP"

#: admin/systems/services/ldap/class_serviceLDAP.inc:48
msgid "LDAP Base"
msgstr "LDAP 베이스"

#: admin/systems/services/ldap/class_serviceLDAP.inc:48
msgid "The LDAP base to use for this LDAP server"
msgstr "이 LDAP 서버에 사용할 LDAP 기반"

#: admin/systems/services/ldap/class_serviceLDAP.inc:53
msgid "LDAP URI"
msgstr "LDAP URI"

#: admin/systems/services/ldap/class_serviceLDAP.inc:53
msgid "The LDAP URI to use in order to contact this LDAP server"
msgstr "이 LDAP 서버에 접속하기 위해 사용할 LDAP URI"

#: admin/systems/services/ldap/class_serviceLDAP.inc:55
msgid "fill-in-your-servers-dns-name"
msgstr "서버 dns 이름을 기입하십시오"

#: admin/systems/services/ldap/class_serviceLDAP.inc:60
msgid "Size limit"
msgstr "크기 제한"

#: admin/systems/services/ldap/class_serviceLDAP.inc:60
msgid "Limit the number of record returned"
msgstr "반환되는 레코드 수 제한"

#: admin/systems/services/ldap/class_serviceLDAP.inc:65
msgid "Time limit"
msgstr "시간 제한"

#: admin/systems/services/ldap/class_serviceLDAP.inc:65
msgid "Time limit for the result to be returned"
msgstr "반환 될 결과의 시간 제한"

#: admin/systems/services/ldap/class_serviceLDAP.inc:70
msgid "Deref"
msgstr "Deref"

#: admin/systems/services/ldap/class_serviceLDAP.inc:70
msgid "Specifies how alias dereferencing is done when performing a search"
msgstr "검색을 수행 할 때 별칭 역 참조가 수행되는 방법을 지정합니다."

#: admin/systems/services/ldap/class_serviceLDAP.inc:75
msgid "TLS Cert"
msgstr "TLS Cert"

#: admin/systems/services/ldap/class_serviceLDAP.inc:75
msgid "Filepath to tls certificate"
msgstr "tls 인증서에 대한 파일 경로"

#: admin/systems/services/ldap/class_serviceLDAP.inc:79
msgid "TLS Key"
msgstr "TLS "

#: admin/systems/services/ldap/class_serviceLDAP.inc:79
msgid "Filepath to tls key"
msgstr "tls 키에 대한 파일 경로"

#: admin/systems/services/ldap/class_serviceLDAP.inc:83
msgid "TLS CaCert"
msgstr "TLS CaCert"

#: admin/systems/services/ldap/class_serviceLDAP.inc:83
msgid "Filepath to tls ca certificate"
msgstr "tls ca 인증서에 대한 파일 경로"

#: admin/systems/services/ldap/class_serviceLDAP.inc:87
msgid "TLS ReqCert"
msgstr "TLS ReqCert"

#: admin/systems/services/ldap/class_serviceLDAP.inc:87
msgid ""
"Specifies what checks to perform on server certificates in a TLS session, if"
" any"
msgstr "TLS 세션에서 서버 인증서에 대해 수행할 검사를 지정합니다."

#: admin/systems/services/ldap/class_serviceLDAP.inc:92
msgid "TLS CrlCheck"
msgstr "TLS CrlCheck"

#: admin/systems/services/ldap/class_serviceLDAP.inc:92
msgid ""
"Specifies if the Certificate Revocation List (CRL) of the CA should be used "
"to verify if the server certificates have  not  been  revoked"
msgstr "서버 인증서가 해지되지 않았는지 확인하기 위해 CA의 인증서 해지 목록 (CRL)을 사용해야하는지 여부를 지정합니다."

#: admin/systems/services/terminal/class_serviceTerminal.inc:27
#: admin/systems/services/terminal/class_serviceTerminal.inc:28
#: admin/systems/services/terminal/class_serviceTerminal.inc:40
msgid "Terminal service"
msgstr "터미널 서비"

#: admin/systems/services/terminal/class_serviceTerminal.inc:43
msgid "Temporary disable login"
msgstr "임시 로그인 사용 중지"

#: admin/systems/services/terminal/class_serviceTerminal.inc:44
msgid "Disables the login on this terminal server"
msgstr "이 터미널 서버에서 로그인을 비활성화합니다."

#: admin/systems/services/terminal/class_serviceTerminal.inc:53
msgid "Supported session types"
msgstr "지원되는 세션 유형"

#: admin/systems/services/terminal/class_serviceTerminal.inc:54
msgid "The session types supported by this terminal server"
msgstr "이 터미널 서버에서 지원하는 세션 유형"

#: admin/systems/class_SystemReleaseColumn.inc:87
#, php-format
msgid "Inherited from %s"
msgstr "%s에서 상속되었습니다."

#: admin/systems/class_serverGeneric.inc:26
#: admin/systems/class_serverGeneric.inc:32
#: admin/systems/class_serverGeneric.inc:33
#: admin/systems/class_systemImport.inc:44
msgid "Server"
msgstr "서버"

#: admin/systems/class_serverGeneric.inc:27
msgid "Server information"
msgstr "서버 정보"

#: admin/systems/class_serverGeneric.inc:47
msgid "server"
msgstr "서버"

#: admin/systems/class_componentGeneric.inc:27
msgid "Component"
msgstr "구성 요소"

#: admin/systems/class_componentGeneric.inc:28
msgid "Component information"
msgstr "구성 요소 정보"

#: admin/systems/class_componentGeneric.inc:32
#: admin/systems/class_componentGeneric.inc:33
msgid "Network device"
msgstr "네트워크 장치"

#: admin/systems/class_componentGeneric.inc:55
msgid "The name of the component"
msgstr "구성 요소 이름"

#: admin/systems/class_componentGeneric.inc:59
msgid "A short description of the component"
msgstr "구성 요소에 대한 간단한 설명"

#: admin/systems/class_systemManagement.inc:43
#: admin/systems/class_systemManagement.inc:47
#: config/systems/class_systemsPluginConfig.inc:28
#: addons/dashboard/class_dashBoardSystems.inc:30
msgid "Systems"
msgstr "시스템"

#: admin/systems/class_systemManagement.inc:44
msgid "Systems Management"
msgstr "시스템 관리"

#: admin/systems/class_systemManagement.inc:45
msgid "Manage servers, computers, printers, phones and other devices"
msgstr "서버, 컴퓨터, 프린터, 전화 및 기타 장치 관리"

#: admin/systems/class_systemManagement.inc:86
msgid "Trigger action"
msgstr "트리거 동작"

#: admin/systems/class_systemManagement.inc:92
msgid "Schedule action"
msgstr "게획된 동작"

#: admin/systems/class_systemManagement.inc:101
msgid "Ping"
msgstr "Ping"

#: admin/systems/class_systemManagement.inc:133
#: admin/systems/class_systemManagement.inc:213
#, php-format
msgid "System %s has no mac address defined, cannot trigger action"
msgstr "시스템 %s에 MAC 주소가 정의되지 않아 작업을 실행할 수 없습니다."

#: admin/systems/class_systemManagement.inc:155
#, php-format
msgid "System %s is currently installing"
msgstr "시스템 %s이(가) 현재 설치 중입니다."

#: admin/systems/class_systemManagement.inc:182
#: admin/systems/class_systemManagement.inc:187
msgid "Action triggered"
msgstr "실행 된 작업"

#: admin/systems/class_systemManagement.inc:182
#, php-format
msgid "Action called without error (results were \"%s\")"
msgstr "오류없이 호출된 작업 (결과는 \"%s\")"

#: admin/systems/class_systemManagement.inc:187
#, php-format
msgid "Action called without error (result was \"%s\")"
msgstr "오류없이 호출된 작업 (결과는 \"%s\")"

#: admin/systems/class_systemManagement.inc:248
msgid "Ping results"
msgstr "Ping 결과"

#: admin/systems/class_systemImport.inc:40
#: admin/systems/class_systemImport.inc:60
msgid "Import"
msgstr "가져오기"

#: admin/systems/class_systemImport.inc:44
msgid "The server you wish to import hosts from"
msgstr "호스트를 가져올 서버"

#: admin/systems/class_systemImport.inc:48
msgid "Type of objects you wish to import"
msgstr "가져올 개체 유형"

#: admin/systems/class_systemImport.inc:53
msgid "Template"
msgstr "템플릿"

#: admin/systems/class_systemImport.inc:53
msgid "Select a template to apply to imported entries"
msgstr "가져온 항목에 적용할 템플릿을 선택하십시오."

#: admin/systems/class_systemImport.inc:107
#, php-format
msgid "Could not parse %s"
msgstr "%s을 (를) 파싱하지 못했습니다."

#: admin/systems/class_systemImport.inc:172
#, php-format
msgid "No DHCP server found for IPs %s"
msgstr "IP %s에 대해 DHCP 서버를 찾을 수 없습니다"

#: admin/systems/class_systemImport.inc:191
#, php-format
msgid "No DNS server found for zone %s"
msgstr "영역 %s에 대한 DNS 서버를 찾을 수 없습니다"

#: admin/systems/class_mobilePhoneGeneric.inc:28
#: admin/systems/class_mobilePhoneGeneric.inc:34
#: admin/systems/class_mobilePhoneGeneric.inc:35
msgid "Mobile phone"
msgstr "휴대 전화"

#: admin/systems/class_mobilePhoneGeneric.inc:29
msgid "Mobile phone information"
msgstr "휴대 전화 정보"

#: admin/systems/class_mobilePhoneGeneric.inc:57
msgid "The name of the mobile phone"
msgstr "휴대 전화의 이름"

#: admin/systems/class_mobilePhoneGeneric.inc:61
msgid "A short description of the mobile phone"
msgstr "휴대 전화에 대한 간단한 설명"

#: admin/systems/class_mobilePhoneGeneric.inc:72
msgid "The serial number of the mobile phone"
msgstr "휴대 전화의 일련 번호"

#: admin/systems/class_mobilePhoneGeneric.inc:76
msgid "IMEI Number"
msgstr "IMEI 번호"

#: admin/systems/class_mobilePhoneGeneric.inc:76
msgid "The IMEI number of the mobile phone"
msgstr "휴대 전화의 IMEI 번호"

#: admin/systems/class_mobilePhoneGeneric.inc:81
msgid "OS"
msgstr "OS"

#: admin/systems/class_mobilePhoneGeneric.inc:81
msgid "The Operating System installed on this phone"
msgstr "이 전화기에 설치된 운영 체제"

#: admin/systems/class_mobilePhoneGeneric.inc:87
msgid "SimCard"
msgstr "심 카드"

#: admin/systems/class_mobilePhoneGeneric.inc:90
msgid "The telephone number of the mobile phone"
msgstr "휴대 전화의 전화 번호"

#: admin/systems/class_mobilePhoneGeneric.inc:94
msgid "PUK Number"
msgstr "PUK 번"

#: admin/systems/class_mobilePhoneGeneric.inc:94
msgid "The PUK number of the simcard in this mobile phone"
msgstr "이 휴대 전화의 SIM 카드의 PUK 번호"

#: config/systems/class_systemsPluginConfig.inc:29
msgid "Systems plugin configuration"
msgstr "시스템 플러그인 구성"

#: config/systems/class_systemsPluginConfig.inc:42
msgid "LDAP tree for systems"
msgstr "시스템 용 LDAP 트리"

#: config/systems/class_systemsPluginConfig.inc:45
msgid "Systems RDN"
msgstr "시스템 RDN"

#: config/systems/class_systemsPluginConfig.inc:45
msgid "Branch in which systems will be stored"
msgstr "시스템이 저장 될 브랜치"

#: config/systems/class_systemsPluginConfig.inc:50
msgid "Server RDN"
msgstr "서버 RDN"

#: config/systems/class_systemsPluginConfig.inc:50
msgid "Branch in which servers will be stored"
msgstr "서버가 저장 될 브랜"

#: config/systems/class_systemsPluginConfig.inc:55
msgid "Workstations RDN"
msgstr "워크스테이션 RDN"

#: config/systems/class_systemsPluginConfig.inc:55
msgid "Branch in which workstations will be stored"
msgstr "워크스테이션을 저장할 브랜치"

#: config/systems/class_systemsPluginConfig.inc:60
msgid "Terminal RDN"
msgstr "터미널 RDN"

#: config/systems/class_systemsPluginConfig.inc:60
msgid "Branch in which terminals will be stored"
msgstr "터미널이 저장 될 브랜"

#: config/systems/class_systemsPluginConfig.inc:65
msgid "Printer RDN"
msgstr "프린터 RDN"

#: config/systems/class_systemsPluginConfig.inc:65
msgid "Branch in which printers will be stored"
msgstr "프린터를 저장할 브랜"

#: config/systems/class_systemsPluginConfig.inc:70
msgid "Component RDN"
msgstr "구성 요소 RDN"

#: config/systems/class_systemsPluginConfig.inc:70
msgid "Branch in which network devices will be stored"
msgstr "네트워크 장치를 저장할 브랜"

#: config/systems/class_systemsPluginConfig.inc:75
msgid "Phone RDN"
msgstr "전화 RDN"

#: config/systems/class_systemsPluginConfig.inc:75
msgid "Branch in which phones will be stored"
msgstr "전화가 저장 될 브랜"

#: config/systems/class_systemsPluginConfig.inc:80
msgid "Mobile phone RDN"
msgstr "휴대 전화 RDN"

#: config/systems/class_systemsPluginConfig.inc:80
msgid "Branch in which mobile phones will be stored"
msgstr "휴대 전화가 저장 될 브랜치"

#: config/systems/class_systemsPluginConfig.inc:87
msgid "Miscellaneous"
msgstr "기타"

#: config/systems/class_systemsPluginConfig.inc:91
msgid "Available encodings for share services"
msgstr "공유 서비스에 사용할 수있는 인코딩"

#: config/systems/class_systemsPluginConfig.inc:95
msgid "Encoding"
msgstr "인코"

#: config/systems/class_systemsPluginConfig.inc:95
msgid "The encoding code name"
msgstr "인코딩 코드 이름"

#: config/systems/class_systemsPluginConfig.inc:100
msgid "Label"
msgstr "라"

#: config/systems/class_systemsPluginConfig.inc:100
msgid "The encoding displayed name"
msgstr "인코딩된 이름 표시"

#: config/systems/class_systemsPluginConfig.inc:108
msgid "Encodings"
msgstr "인코딩"

#: config/systems/class_systemsPluginConfig.inc:121
msgid "Mandatory IP"
msgstr "필수 IP"

#: config/systems/class_systemsPluginConfig.inc:121
msgid "Object types tabs for which IP field should be mandatory"
msgstr "IP 필드가 필수인 객체 유형 탭"

#: addons/dashboard/class_dashBoardSystems.inc:31
msgid "Statistics and information about systems"
msgstr "시스템에 대한 통계 및 정보"

#: addons/dashboard/class_dashBoardSystems.inc:43
msgid "Statistics"
msgstr "통계"

#: addons/dashboard/class_dashBoardSystems.inc:48
msgid "Computer name to use by unit"
msgstr "장치에서 사용할 컴퓨터 이름"

#: addons/dashboard/class_dashBoardSystems.inc:73
msgid "Workstations"
msgstr "워크스테이션"

#: addons/dashboard/class_dashBoardSystems.inc:77
msgid "Servers"
msgstr "서버"

#: addons/dashboard/class_dashBoardSystems.inc:81
msgid "Windows Workstations"
msgstr "Windows 워크스테이션"

#: addons/dashboard/class_dashBoardSystems.inc:86
msgid "Terminals"
msgstr "터미널"

#: addons/dashboard/class_dashBoardSystems.inc:90
msgid "Printers"
msgstr "프린터"

#: addons/dashboard/class_dashBoardSystems.inc:94
msgid "Phones"
msgstr "전화"

#: addons/dashboard/class_dashBoardSystems.inc:98
msgid "Components"
msgstr "구성 요소"

#: addons/dashboard/class_dashBoardSystems.inc:102
msgid "Mobile phones"
msgstr "휴대 전화"

#: addons/dashboard/class_dashBoardSystems.inc:117
#, php-format
msgid ""
"Statistics for type \"%s\" could not be computed because of the following "
"error: %s"
msgstr "다음 오류로 인해 유형 \"%s\"의 통계를 계산할 수 없습니다 : %s"

#: addons/dashboard/class_dashBoardNetwork.inc:27
msgid "Network"
msgstr "네트워크"

#: addons/dashboard/class_dashBoardNetwork.inc:28
msgid "Statistics and various information"
msgstr "통계 및 각종 정보"

#: addons/dashboard/class_dashBoardNetwork.inc:40
msgid "DHCP"
msgstr "DHCP"

#: addons/dashboard/class_dashBoardNetwork.inc:45
msgid "DNS"
msgstr "DNS"

#: addons/dashboard/class_dashBoardNetwork.inc:81
#, php-format
msgid ""
"Statistics for DHCP could not be computed because of the following error: %s"
msgstr "다음 오류로 인해 DHCP 통계를 계산할 수 없습니다 : %s"

#: admin/systems/server_import.tpl.c:2
msgid ""
"Warning : Once you import your OPSI hosts into FusionDirectory, they will be"
" managed by it, so if you delete them or deactivate their OPSI tab, they "
"will be removed from OPSI."
msgstr ""
"경고 : OPSI 호스트를 FusionDirectory로 가져 오면 호스트가 이를 관리하므로 OPSI를 삭제하거나 OPSI 탭을 "
"비활성화하면 OPSI에서 제거됩니다."

#: addons/dashboard/systems_stats.tpl.c:2
msgid "There are several argonaut servers! (this is not yet supported)"
msgstr "argonaut 서버가 여러 개 있습니다! (아직 지원되지 않음)"

#: addons/dashboard/systems_stats.tpl.c:5
msgid "There is an argonaut server running on %1 (%3://%2:%4)"
msgstr "실행중인 argonaut 서버가 있습니다. %1 (%3://%2:%4)"

#: addons/dashboard/systems_stats.tpl.c:8
msgid "Only one system is configured to run an argonaut client."
msgid_plural "%1 systems are configured to run an argonaut client."
msgstr[0] "%1 시스템이 argonaut 클라이언트를 실행하도록 구성되었습니다."

#: addons/dashboard/systems_stats.tpl.c:11
msgid "But no system is configured to run an argonaut client!"
msgstr "argonaut 클라이언트를 실행하는 시스템은 구성되어 있지 않습니다!"

#: addons/dashboard/systems_stats.tpl.c:14
msgid ""
"A system is configured to run an argonaut client, but there is no argonaut "
"server configured!"
msgid_plural ""
"%1 systems are configured to run an argonaut client, but there is no "
"argonaut server configured!"
msgstr[0] "%1 시스템이 argonaut 클라이언트를 실행하도록 구성되었지만 argonaut 서버가 구성되지 않았습니다!"
